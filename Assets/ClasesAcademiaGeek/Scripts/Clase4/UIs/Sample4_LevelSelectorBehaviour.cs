﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample4_LevelSelectorBehaviour : Sample4_UIBase
{
    private Transform parentView;

    public void OpenMainMenu()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.Main, UIState.Open);
    }

    public void ChangeView(UIView view, UIState viewState)
    {
        Sample4_FactoryView.CreateView(view, parentView);
        CloseView();
    }
    public override void CloseView()
    {
        base.CloseView();
    }

}
