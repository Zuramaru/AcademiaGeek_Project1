﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample4_SettingsBehaviour : Sample4_UIBase
{
    private Transform parentView;

    public void OpenHUD()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.HUD, UIState.Open);
    }

    public void ChangeView(UIView view, UIState viewState)
    {
        Sample4_FactoryView.CreateView(view, parentView);
        CloseView();
    }
    public override void CloseView()
    {
        base.CloseView();
    }
}
