﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

//using utils;

/// <summary>
/// Singlenton Administrador XML
/// </summary>
public class XMLManager
{
	private static XmlDocument xmlDocument;
	private static TextAsset textXML;

	public static List<Message> Messages;

	static XMLManager ()
	{


	}

	public static void LoadXML (string xmlName)
	{
		textXML = Resources.Load ("XML/" + xmlName) as TextAsset;

		xmlDocument = new XmlDocument ();

		xmlDocument.LoadXml (textXML.ToString ());
        
    }

	public static void LoadMessages ()
	{
        string numberMessageValue = xmlDocument.DocumentElement.SelectSingleNode("/Messages/NumberMessages").InnerText;
        int numberMessages = int.Parse(numberMessageValue);

        Messages = new List<Message>();
        Messages.Clear();
        //Debug.Log("NumberMessages: "+ numberMessages);
        for (int i = 1; i <= numberMessages; i++)
        {
            Message message = new Message();
            message.message = xmlDocument.DocumentElement.SelectSingleNode("/Messages/Message"+i).InnerText;
            Messages.Add(message);
            //Debug.Log("Posicion "+i+" "+Messages[i-1].message);
        }
	}

	
}

