﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private bool canmove = true;
    public float speed = 10.0F; //Velocidad de movimiento
    public float rotationSpeed = 100.0F; //Velocidad de rotación

    public void CanotMove()
    {
        canmove = !canmove;
    }
    void Update()
    {
        if (canmove)
        {
            transform.Translate(0, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime);
            transform.Rotate(0, Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime, 0);
        }
    }
}
