﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker1 : MonoBehaviour
{
    private Player player;
    private Rotations Rotats;
    private float delayToRotate = 15;
    public bool rotating;

    private void Awake()
    {
        player = GetComponent<Player>();
        Rotats = GetComponent<Rotations>();
    }
    public void Update()
    {
        if (rotating)
            return;
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -transform.up, out hit))
        {
            Debug.DrawRay(transform.position, -transform.up, Color.red);
            Debug.Log(hit.collider.name);
        }
        else
        {
            player.CanotMove();
            rotating = !rotating;
            Debug.DrawRay(transform.position, -Vector3.up, Color.red);
            StartCoroutine(Rotation(Vector3.right, 90));
        }
    }
    private IEnumerator Rotation(Vector3 referenceAxis, float finalAngle)
    {
        float valueToRotate = 0;
        while (valueToRotate < finalAngle)
        {
            transform.Rotate(referenceAxis, delayToRotate, Space.Self);
            valueToRotate += delayToRotate;
            yield return new WaitForSeconds(0.1f);
        }
        rotating = !rotating;
        player.CanotMove();
    }

}
