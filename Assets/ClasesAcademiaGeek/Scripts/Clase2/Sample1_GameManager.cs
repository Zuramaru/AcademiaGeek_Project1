﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Sample1_GameManager : MonoBehaviour
{
    public Sample1_ActionBase lover1;
    public Sample1_ActionBase lover2;

    public Sample1_CheckerPathManager checker;

    /// <summary>
    /// Actions Temp
    /// </summary>
    private Sample1_CubeActions cubeActionsLover1;
    private Sample1_CubeActions cubeActionsLover2;

    private bool isFinishFirstActionLover1;
    private bool isFinishFirstActionLover2;

    private bool isGameplayActive = false;

    private bool isLover1EncounterLover2;
    private bool isLover2EncounterLover1;

    private void Start()
    {
        Sample1_ActionBase.CompleteAction += OnCompleteAction;
    }

    private void StartGameplay()
    {
        isFinishFirstActionLover1 = false;
        isFinishFirstActionLover2 = false;

        isLover1EncounterLover2 = false;
        isLover2EncounterLover1 = false;

        GameplayStates(Gameplay_States.Checking, lover1);
        GameplayStates(Gameplay_States.Checking, lover2);
    }

    private void GameplayStates(Gameplay_States states, Sample1_ActionBase lover)
    {
        switch (states)
        {
            case Gameplay_States.Checking:
                if (!checker.LoverChecker(lover.transform))
                {
                    if (lover.character == Characters.Lover1)
                    {
                        cubeActionsLover1 = CheckingMode(lover);
                    }else
                    {
                        cubeActionsLover2 = CheckingMode(lover);
                    }
                    GameplayStates(Gameplay_States.Movement, lover);
                }
                else
                {
                    GameplayStates(Gameplay_States.Win, lover);
                }
                break;
            case Gameplay_States.Movement:
                MovementMode(lover);
                break;

            case Gameplay_States.Win:
                if (checker.CubeChecker(lover.transform).action1 != Characters_Actions.No_Ground)
                {
                    ActiveCharacterAction(lover, Characters_Actions.Move_Win);
                }
                break;
            default:
                break;
        }
    }

    private Sample1_CubeActions CheckingMode(Sample1_ActionBase lover)
    {
        Sample1_CubeActions cubeAction;
        cubeAction = checker.CubeChecker(lover.transform);
        Debug.Log(lover.character + " Checking Encounter: Action1: " + cubeAction.action1 + " Action2: " + cubeAction.action2);
        return cubeAction;
    }

    private void MovementMode(Sample1_ActionBase lover)
    {
        switch (lover.character)
        {
            case Characters.Lover1:
                if (!isFinishFirstActionLover1)
                {
                    ActiveCharacterAction(lover1, cubeActionsLover1.action1);
                }
                else
                {
                    ActiveCharacterAction(lover1, cubeActionsLover1.action2);
                }
                break;
            case Characters.Lover2:
                if (!isFinishFirstActionLover2)
                {
                    ActiveCharacterAction(lover2, cubeActionsLover2.action1);
                }
                else
                {
                    ActiveCharacterAction(lover2, cubeActionsLover2.action2);
                }
                break;
            default:
                break;

        }
    }

    private void ActiveCharacterAction(Sample1_ActionBase lover, Characters_Actions action)
    {
        Debug.Log(lover.name + " : " + action);
        switch (action)
        {
            case Characters_Actions.Move_Forward:
                lover.GetComponent<Sample1_Movement>().BeginAction(action);
                break;
            case Characters_Actions.Move_Win:
                lover.GetComponent<Sample1_Movement>().BeginAction(action);
                break;
            case Characters_Actions.Turn_Left:
                lover.GetComponent<Sample1_Rotation>().BeginAction(action);
                break;
            case Characters_Actions.Turn_Right:
                lover.GetComponent<Sample1_Rotation>().BeginAction(action);
                break;
            case Characters_Actions.Turn_Forward:
                lover.GetComponent<Sample1_Rotation>().BeginAction(action);
                break;
            case Characters_Actions.No_Action:
                OnCompleteAction(lover, action);
                break;
            case Characters_Actions.No_Ground:
                Debug.Log("No Ground");
                break;
            default:
                break;

        }
    }

    private void OnCompleteAction(Sample1_ActionBase lover, Characters_Actions action)
    {

        if(lover.character == Characters.Lover1)
        {
            if (action != Characters_Actions.Move_Win)
            {
                if (!isFinishFirstActionLover1)
                {
                    isFinishFirstActionLover1 = true;
                    GameplayStates(Gameplay_States.Movement, lover);
                }
                else
                {
                    isFinishFirstActionLover1 = false;
                    GameplayStates(Gameplay_States.Checking, lover);
                }
            }else
            {
                Debug.Log(lover.character + " Ha encontrado a su lover");
                WinMode();
            }
        }

        if(lover.character == Characters.Lover2)
        {
            if (action != Characters_Actions.Move_Win)
            {
                if (!isFinishFirstActionLover2)
                {
                    isFinishFirstActionLover2 = true;
                    GameplayStates(Gameplay_States.Movement, lover);
                }
                else
                {
                    isFinishFirstActionLover2 = false;
                    GameplayStates(Gameplay_States.Checking, lover);
                }
            }
            else
            {
                Debug.Log(lover.character + " Ha encontrado a su lover");
                WinMode();
            }
        }

        
    }

    private void WinMode()
    {
        if(isLover1EncounterLover2 && isLover2EncounterLover1)
        {
            Debug.Log("WinMode");
            Sample1_ActionBase.CompleteAction -= OnCompleteAction;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (!isGameplayActive)
            {
                isGameplayActive = true;
                StartGameplay();
            }
        }
    }
}
