﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Sample1_Movement : Sample1_ActionBase
{
    private float maxValueToNormalMove = 1;
    private float maxValueToWin = 0.5f;
    private float delayToMove = 0.1f;

    public override void BeginAction(Characters_Actions action)
    {
        Debug.Log(character + " BeginAction: " + action);
        base.BeginAction(action);
        switch (action)
        {
            case Characters_Actions.Move_Forward:
                StartCoroutine(Movement(maxValueToNormalMove,action));
                break;
            case Characters_Actions.Move_Win:
                StartCoroutine(Movement(maxValueToWin,action));
                break;
            default:
                break;
        }
    }

    private IEnumerator Movement(float maxMovement, Characters_Actions action)
    {
        
        float valueToMove = 0;
        while (valueToMove < maxMovement)
        {
            transform.Translate(transform.forward * delayToMove, Space.World);
            valueToMove += delayToMove;
            yield return new WaitForSeconds(delayToMove);
        }
        
        EndAction(action);
    }

    protected override void EndAction(Characters_Actions action)
    {
        Debug.Log(character + " EndAction: " + action);
        base.EndAction(action);
    }
}
